default['vault-agent']['version']     = '1.11.3'
default['vault-agent']['checksum']    = '322393aee141c4711fc5f9e1df9f461af3a861e59b8d4d0a85e82477cdbc73a0'
default['vault-agent']['binary_url']  = "https://releases.hashicorp.com/vault/#{node['vault-agent']['version']}/vault_#{node['vault-agent']['version']}_linux_amd64.zip"

default['vault-agent']['dir'] = '/opt/vault'
default['vault-agent']['run_dir'] = '/var/run/vault'
default['vault-agent']['config_dir'] = '/etc/vault.d'
default['vault-agent']['binary'] = '/usr/bin/vault'
default['vault-agent']['certs_dir'] = '/etc/vault.d'
default['vault-agent']['flags']['config'] = "#{node['vault-agent']['config_dir']}/vault-agent.hcl"

default['vault-agent']['user']        = 'vault'
default['vault-agent']['group']       = 'vault'

default['vault-agent']['vault_addr']  = 'https://vault.gke.gitlab.net'
