# Cookbook Name:: gitlab-vault-agent
# Recipe:: default
# License:: MIT
#
# Copyright 2016, GitLab Inc.

## Create required system user
user node['vault-agent']['user'] do
  system true
  shell '/bin/false'
  home node['vault-agent']['config_dir']
  not_if node['vault-agent']['user'] == 'root'
end

## Install
package 'Verify unzip is installed' do
  package_name 'unzip'
end
ark 'vault' do
  url node['vault-agent']['binary_url']
  checksum node['vault-agent']['checksum']
  version node['vault-agent']['version']
  prefix_root Chef::Config['file_cache_path']
  path node['vault-agent']['dir']
  creates 'vault'
  action :dump
  notifies :restart, 'service[vault-agent]', :delayed
end

directory node['vault-agent']['run_dir'] do
  owner node['vault-agent']['user']
  group node['vault-agent']['group']
  mode '0755'
  recursive true
end

directory node['vault-agent']['config_dir'] do
  owner node['vault-agent']['user']
  group node['vault-agent']['group']
  mode '0755'
  recursive true
end

directory node['vault-agent']['certs_dir'] do
  owner node['vault-agent']['user']
  group node['vault-agent']['group']
  mode '0755'
  recursive true
end

# Set up configuration

template node['vault-agent']['flags']['config'] do
  source 'vault-agent.hcl.erb'
  path node['vault-agent']['flags']['config']
  owner node['vault-agent']['user']
  group node['vault-agent']['group']
  mode '0644'
  notifies :restart, 'service[vault-agent]'
end

# Set up systemd service
systemd_unit 'vault-agent.service' do
  content(
    Unit: {
      Description: 'HashiCorp Vault - A tool for managing secrets',
      Documentation: 'https://www.vaultproject.io/docs/',
      Requires: 'network-online.target',
      After: 'network-online.target',
      ConditionFileNotEmpty: '/etc/vault.d/vault-agent.hcl',
      StartLimitBurst: '3',
    },
    Service: {
      User: 'vault',
      Group: 'vault',
      ProtectSystem: 'no',
      PrivateTmp: 'yes',
      PrivateDevices: 'yes',
      SecureBits: 'keep-caps',
      AmbientCapabilities: 'CAP_IPC_LOCK',
      NoNewPrivileges: 'yes',
      Environment: 'VAULT_ADDR=' + node['vault-agent']['vault_addr'] + ' NODENAME=' + node['fqdn'],
      PassEnvironment: 'VAULT_ADDR NODENAME',
      ExecStart: '/opt/vault/vault agent -config=/etc/vault.d/vault-agent.hcl -tls-skip-verify',
      ExecReload: '/bin/kill --signal HUP $MAINPID',
      KillMode: 'process',
      KillSignal: 'SIGINT',
      Restart: 'on-failure',
      RestartSec: '5',
      TimeoutStopSec: '30',
      StartLimitInterval: '60',
      StartLimitBurst: '3',
      LimitNOFILE: '65536',
      LimitMEMLOCK: 'infinity',
    },
    Install: {
      WantedBy: 'multi-user.target',
    }
  )

  action %i(create)
  notifies :restart, 'service[vault-agent]', :delayed
end

# vault-agent is started in its package's postinst.
service 'vault-agent' do
  action :enable
end
