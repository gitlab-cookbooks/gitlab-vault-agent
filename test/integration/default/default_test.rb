# InSpec tests for recipe gitlab-vault-agent::default

control 'general-checks' do
  impact 1.0
  title 'General tests for gitlab-vault-agent cookbook'
  desc '
    This control ensures that:
      * there is no duplicates in /etc/group'

  describe etc_group do
    its('gids') { should_not contain_duplicates }
  end
end

control 'general-vault-agent-checks' do
  impact 1.0
  title 'General tests for vault agent service'
  desc '
    This control ensures that:
      * vault-agent package is installed
      * vault-agent service is enabled
      * vault-agent service is running'

  describe package('vault-agent') do
    it { should be_installed }
  end

  describe service('vault-agent') do
    it { should be_enabled }
  end

  describe service('vault-agent') do
    it { should be_running }
  end
end

control 'vault-agent-config-checks' do
  impact 1.0
  title 'Tests vault-agent settings'
  desc '
    This control ensures that:
      * config files exist'

  describe file('/etc/vault.d//vault-agent.hcl') do
    its('mode') { should cmp '0644' }
  end
end
