# Cookbook:: gitlab-vault-agent
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab-vault-agent::default' do
  context 'when all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      # SoloRunner is recommended because it is faster, but if you need a
      # feature that is not supported by SoloRunner then try ServerRunner.
      ChefSpec::SoloRunner.new(
        platform: 'ubuntu',
        version: '16.04'
      ).converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end
  end
end
