name             'gitlab-vault-agent'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'GitLab cookbook for Vault Agent'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'
chef_version     '>= 14.0' if respond_to?(:chef_version)
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_users/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_users'
supports         'ubuntu', '= 18.04'

# Please specify dependencies with version pin:
# depends 'cookbookname', '~> 1.0.0'
depends 'ark', '~> 5.0.0'
depends 'seven_zip', '~> 3.2.0'
